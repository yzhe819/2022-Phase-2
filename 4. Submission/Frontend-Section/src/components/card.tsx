import { ItemDetails } from "../types/details";

function Card(details: { item: ItemDetails }) {
  const item = details.item;
  return (
    <div
      className="w-full cursor-pointer card glass"
      onClick={() => {
        if (item.links) window.open(item.links[0].href, "_blank");
      }}
    >
      <figure className="h-40">
        <img
          src={item.links?.[0].href || ""}
          alt={item.data[0].title}
          className="w-full h-full bg-center bg-cover"
        />
      </figure>
      <div className="card-body">
        <h2 className="card-title">{item.data[0].title}</h2>
        <p className="line-clamp-3">{item.data[0].description}</p>
        {item.data[0].keywords && (
          <div className="flex flex-row flex-wrap gap-2 mt-2">
            {item.data[0].keywords.map((keyword: any) => (
              <div className="badge badge-primary badge-outline line-clamp-1">
                {keyword}
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
  );
}

export default Card;
