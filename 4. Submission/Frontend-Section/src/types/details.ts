export interface ItemDetails {
  links: { href: string | undefined }[];
  data: {
    nasa_id: string;
    title: string;
    description: string;
    keywords: any[];
  }[];
}
