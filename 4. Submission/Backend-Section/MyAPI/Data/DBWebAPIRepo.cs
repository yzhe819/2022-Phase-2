﻿namespace MyAPI.Data;

public class DBWebAPIRepo : IWebAPIRepo
{
    private readonly WebAPIDBContext _dbcontext;

    public DBWebAPIRepo(WebAPIDBContext dbcontext)
    {
        _dbcontext = dbcontext;
    }

    public Customer AddCustomer(Customer customer)
    {
        var e = _dbcontext.Customers.Add(customer);
        var c = e.Entity;
        _dbcontext.SaveChanges();
        return c;
    }

    public Customer GetCustomerByID(int id)
    {
        var customer = _dbcontext.Customers.FirstOrDefault(e => e.Id == id);
        return customer;
    }

    public IEnumerable<Customer> GetCustomers()
    {
        IEnumerable<Customer> customers = _dbcontext.Customers.ToList();
        return customers;
    }

    public Customer UpdateCustomer(Customer customer)
    {
        var e = _dbcontext.Customers.Update(customer);
        var c = e.Entity;
        _dbcontext.SaveChanges();
        return c;
    }

    public void DeleteCustomerByID(int id)
    {
        var customer = GetCustomerByID(id);
        _dbcontext.Customers.Remove(customer);
        _dbcontext.SaveChanges();
    }
}