using Microsoft.EntityFrameworkCore;

namespace MyAPI.Data;

public class WebAPIDBContext : DbContext
{
    public WebAPIDBContext(DbContextOptions<WebAPIDBContext> options) : base(options)
    {
    }

    public DbSet<Customer> Customers { get; set; }
}