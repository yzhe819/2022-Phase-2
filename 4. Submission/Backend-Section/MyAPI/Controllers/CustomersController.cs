﻿using Microsoft.AspNetCore.Mvc;
using MyAPI.Data;
using MyAPI.Dtos;

[ApiController]
[Route("webapi")]
public class CustomersController : Controller
{
    private readonly IWebAPIRepo _repo;

    public CustomersController(IWebAPIRepo repo)
    {
        _repo = repo;
    }

    [HttpGet("GetCustomers")]
    public ActionResult<IEnumerable<Customer>> GetCustomers()
    {
        var res = _repo.GetCustomers();
        var c = res.Select(e => new CustomerOutDto { Id = e.Id, FirstName = e.FirstName, LastName = e.LastName });
        return Ok(c);
    }

    [HttpGet("GetCustomer/{id}")]
    public ActionResult<IEnumerable<Customer>> GetCustomerByID(int id)
    {
        var customer = _repo.GetCustomerByID(id);
        if (customer == null) return NotFound();
        var c = new CustomerOutDto { Id = customer.Id, FirstName = customer.FirstName, LastName = customer.LastName };
        return Ok(c);
    }

    [HttpPost("AddCustomer")]
    public ActionResult<CustomerOutDto> AddCustomer(CustomerInputDto customer)
    {
        var c = new Customer
        {
            FirstName = customer.FirstName,
            LastName = customer.LastName, Email = customer.Email
        };
        var addedCustomer = _repo.AddCustomer(c);
        var co = new CustomerOutDto
        {
            Id = addedCustomer.Id,
            FirstName = addedCustomer.FirstName,
            LastName = addedCustomer.LastName
        };
        return CreatedAtAction(nameof(GetCustomerByID), new { id = co.Id }, co);
    }

    [HttpPut("UpdateCustomer/{id}")]
    public ActionResult<Customer> UpdateCustomerByID(int id, CustomerInputDto customer)
    {
        var old = _repo.GetCustomerByID(id);
        if (old == null) return NotFound();
        old.LastName = customer.LastName;
        old.FirstName = customer.FirstName;
        old.Email = customer.Email;
        var c = _repo.UpdateCustomer(old);
        return Ok(c);
    }

    [HttpDelete("DeleteCustomer/{id}")]
    public ActionResult<IEnumerable<Customer>> DeleteCustomerByID(int id)
    {
        _repo.DeleteCustomerByID(id);
        return Ok();
    }
}