using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("time")]
public class WorldTimeController : Controller
{
    private readonly HttpClient _client;

    /// <summary />
    public WorldTimeController(IHttpClientFactory clientFactory)
    {
        if (clientFactory is null) throw new ArgumentNullException(nameof(clientFactory));
        _client = clientFactory.CreateClient("WorldTimeAPI");
    }

    /// <summary>
    ///     Get the current time of the address, such as `auckland`
    /// </summary>
    /// <returns>A JSON object representing the related time info</returns>
    [HttpGet]
    [Route("search/{address}")]
    [ProducesResponseType(200)]
    public async Task<IActionResult> SearchPokemon(string address)
    {
        var res = await _client.GetAsync(address);
        var content = await res.Content.ReadFromJsonAsync<object>();
        return Ok(content);
    }
}