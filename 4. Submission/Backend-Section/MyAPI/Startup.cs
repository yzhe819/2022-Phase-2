using Microsoft.EntityFrameworkCore;
using MyAPI.Data;

namespace MyAPI;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllers();
        // setup swagger ui
        services.AddSwaggerDocument(options =>
        {
            options.DocumentName = "My Amazing API";
            options.Version = "V1";
        });
        // setup database
        services.AddDbContext<WebAPIDBContext>(options =>
            options.UseSqlite(Configuration.GetConnectionString("WebAPIConnection")));
        services.AddScoped<IWebAPIRepo, DBWebAPIRepo>();
        // setup apis
        services.AddHttpClient("WorldTimeAPI",
            client => { client.BaseAddress = new Uri("http://worldtimeapi.org/api/timezone/Pacific/"); });
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
            app.UseOpenApi();
            app.UseSwaggerUi3();
        }

        app.UseHttpsRedirection();

        app.UseRouting();

        app.UseAuthentication();
        app.UseAuthorization();

        app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
    }
}