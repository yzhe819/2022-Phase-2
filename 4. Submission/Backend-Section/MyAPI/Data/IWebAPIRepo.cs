﻿namespace MyAPI.Data;

public interface IWebAPIRepo
{
    IEnumerable<Customer> GetCustomers();
    Customer GetCustomerByID(int id);
    Customer AddCustomer(Customer customer);
    Customer UpdateCustomer(Customer customer);
    void DeleteCustomerByID(int id);
}