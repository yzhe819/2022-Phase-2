using Microsoft.EntityFrameworkCore;
using MyAPI.Data;

namespace MyAPITests;

public class Tests
{
    private DBWebAPIRepo _dbWebApiRepo;

    [SetUp]
    public void Setup()
    {
        var options = new DbContextOptionsBuilder<WebAPIDBContext>()
            .UseInMemoryDatabase("MemoryTesting").Options;

        var context = new WebAPIDBContext(options);
        _dbWebApiRepo = new DBWebAPIRepo(context);
    }


    [Test]
    public void TestCRUD()
    {
        TestGetALL();
        TestAdd();
        TestUpdate();
        TestGetById();
        TestDelete();
    }

    public void TestGetALL()
    {
        var list = _dbWebApiRepo.GetCustomers();
        Assert.AreEqual(list.Count(), 0);
    }


    public void TestAdd()
    {
        var list = _dbWebApiRepo.GetCustomers();
        Assert.AreEqual(list.Count(), 0);
        _dbWebApiRepo.AddCustomer(new Customer
        {
            FirstName = "Test1",
            LastName = "Test2",
            Email = "TestEmail"
        });
        list = _dbWebApiRepo.GetCustomers();
        Assert.AreEqual(list.Count(), 1);
        Assert.AreEqual(list.First().FirstName, "Test1");
    }


    public void TestGetById()
    {
        var list = _dbWebApiRepo.GetCustomers();
        Assert.AreEqual(list.Count(), 1);
        var id = list.First().Id;
        var target = _dbWebApiRepo.GetCustomerByID(id);
        Assert.AreEqual(target.FirstName, list.First().FirstName);
    }


    public void TestUpdate()
    {
        var id = _dbWebApiRepo.GetCustomers().First().Id;
        var target = _dbWebApiRepo.GetCustomerByID(id);
        target.FirstName = "This is a new name";
        target = _dbWebApiRepo.GetCustomerByID(id);
        Assert.AreEqual(target.FirstName, "This is a new name");
    }


    public void TestDelete()
    {
        var list = _dbWebApiRepo.GetCustomers();
        Assert.AreEqual(list.Count(), 1);
        var id = list.First().Id;
        _dbWebApiRepo.DeleteCustomerByID(id);
    }
}