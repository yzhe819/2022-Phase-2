# Frontend Section

### Requirements:

- Create a new typescript and react web app
  - I used `npx` to create this project
- Connect this application to a *different* 3rd party RESTful Web API
  - I used the `NASA api` to handle the search logic
- Allow users to input information that will be utilized by the API
  - The search bar 🤪
- Make use of a UI library like Material-UI
  - I used a UI library based on `tailwind css` called `daisyui`
- Utilize source control through GitHub.
  - Check my commit history 🤪

### The actual UI:

![image-20220810174309846](./images/image-20220810174309846.png)

### Enter Mars to search:

![image-20220810174933253](./images/image-20220810174933253.png)





# Backend Section

### Requirements

- Create at least one controller that implements CRUD operations for a resource

  - I created a `CustomersController` to implement CRUD operations, it depends on SQLite database
  - ![image-20220810175709048](./images/image-20220810175709048.png)

- Call at least one other API.

  - I invoke a timezone api “http://worldtimeapi.org/api/timezone/Pacific/”. When the user enters an address to call my `worldTime` endpoint, I further call this timezone api to return the current time for this address.

- Create at least two configuration files, and demonstrate the differences between starting the project with one file over another.

  - Added a debug configuration file, which allows developers to get more information when debugging than when running in a production environment
  - Change the **LogLevel** config

- Demonstrate an understanding of how these middleware via DI (dependency injection) simplifies your code 👇

  - **How middleware via DI (dependency injection) simplifies the code?**

    Dependency injection is for inversion of control (easier to maintain), but simple inversion of control will still leave a lot of dependencies in the code (configuration code will be scattered throughout the application), which will make the code difficult to manage. In order to facilitate code management and maintenance, we need a place to manage all dependencies in the system in a unified manner, and built-in service container `IServiceProvider` was born. are born.

    Services are usually registered in the app's `Startup.ConfigureServices` method. When the project starts up, it will set up related services (a list of middleware classes), such as configure the swagger, database context, http clients and so on. This is convenient as it allows us to define a lot of the behaviour of the application or services in one place, which can also reduce the consumption of instance initialization. (realize better code management)

- Demonstrate the use of NUnit to unit test your code.
  - Create `MyAPITests` project, which is using `Nunit` to test the `MyAPI` project
- Use at least one substitute to test your code
  - use InMemoryDatabase rather than the actual database for the test code. This will not affect the real production environment.
- Demonstrate an understanding of why the middleware libraries made your code easier to test
  - The swagger ui allows developers to quickly preview request results. This is very convenient and saves development time.
