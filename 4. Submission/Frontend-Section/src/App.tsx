import { useEffect, useState } from "react";
import Card from "./components/card";
import Loading from "./components/loading";
import { ItemDetails } from "./types/details";

function App() {
  const [isLoading, setLoading] = useState<boolean>(true);
  const [data, setData] = useState<ItemDetails[]>([]);
  const [searchName, setSearchName] = useState<string>("");

  useEffect(() => {
    setLoading(true);
    fetch("https://images-api.nasa.gov/search?media_type=image")
      .then((res) => res.json())
      .then((data) => {
        setData(data.collection.items);
        setLoading(false);
      });
  }, []);

  const handleSearch = () => {
    setLoading(true);
    fetch(`https://images-api.nasa.gov/search?q=${searchName}`)
      .then((res) => res.json())
      .then((data) => {
        setData(data.collection.items);
        setLoading(false);
      });
  };

  return (
    <div className="flex flex-col max-w-screen-xl gap-12 px-4 mx-auto my-12">
      <div className="flex flex-row items-center justify-start gap-4">
        <input
          type="text"
          placeholder="Type here to search"
          className="flex-1 input input-bordered"
          onChange={(e) => setSearchName(e.target.value)}
        />
        <button className="btn btn-primary" onClick={handleSearch}>
          Search
        </button>
      </div>
      {isLoading ? (
        <div className="flex items-center justify-center w-full py-8">
          <Loading />
        </div>
      ) : (
        <div className="grid grid-cols-3 gap-x-4 gap-y-8">
          {data.map((item) => (
            <Card item={item} key={item.data[0].nasa_id} />
          ))}
        </div>
      )}
    </div>
  );
}

export default App;
